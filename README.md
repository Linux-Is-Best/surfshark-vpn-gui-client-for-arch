# Surfshark VPN GUI Client for Arch


## Getting started

1. [**Download** the latest release](https://gitlab.com/Linux-Is-Best/surfshark-vpn-gui-client-for-arch/-/releases)
1. Extract the archive
1. Open a terminal window in that folder (directory) 
1. Follow the directions below
  
## Pre-requirement

**base-devel** and **git** which you can verify you have or install if needed by enterting the following command: `sudo pacman -S --needed base-devel git` The system will skip if you already have them or install them if needed.

## Install / Upgrade

In your terminal window inside the directory (folder) you extracted the latest release, run the following command:


```
makepkg -si
```

## License

In simple English, this script which you use to install Suftshark VPN's GUI Client is free and open source and provided "as is". I release the script used into the public domain and I welcome anyone to improve upon it and develop it further, including take credit (it is in the public domain)

But the actual client (program) and DEB package that this downloads and converts is owned by Suftshark and they retain all their respected rights.

## Project status

On going... If you notice Surfshark VPN has released a new package (https://ocean.surfshark.com/debian/pool/main/s/surfshark/) please **let me know** and I will update this.

I use [EndeavourOS](https://endeavouros.com/), by the way. - It's Arch Linux made easy. While I ensure the target (vanilla Arch Linux) will work, I am also confident [EndeavourOS](https://endeavouros.com/) will work too.

## Dear Surfshark VPN (if you're listening)

It would be **AWESOME** if you could officially support Arch Linux (and Fedora too). No one realistically expects you to support all the many offshoot distributions. You currently support Debian and Ubuntu, which are the base of many other distros. People who use another distro based on either of those (Debian or Ubuntu) know they are on their own. It would be cool if you did the same for Arch Linux (and Fedora too).  

In my opinion, someone such as myself should be a last resort.
