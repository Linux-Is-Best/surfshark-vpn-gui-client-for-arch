#!/bin/bash

# This script is NOT part of the AUR release and does NOT install or update software.
#
# This script is useful for the developer or anyone who wants to check for an update to Suftshark's DEB repo
# and plans to manually update the AUR themselves.  It checks (1x an hour) for a file name change (or any new files)
# hosted in Surfshark's DEB repo. Once a change is detected, it will notify you, so you can generate a new AUR. 
#
# This script is independent from the AUR and needs to be run manually.
#
# dependencies are curl and libnotify -- sudo pacman -S curl libnotify
# 

URL="https://ocean.surfshark.com/debian/pool/main/s/surfshark/"
TEMP_DIR="/tmp/surfshark_files"

# Initial fetch to get the list of files in the directory
mkdir -p "$TEMP_DIR"
curl -s "$URL" > "$TEMP_DIR/surfshark_files_initial.txt"

while true; do
    # Fetch the latest list of files in the directory
    curl -s "$URL" > "$TEMP_DIR/surfshark_files_current.txt"

    # Compare the current file list with the previous one
    diff_output=$(diff -u "$TEMP_DIR/surfshark_files_initial.txt" "$TEMP_DIR/surfshark_files_current.txt")

    if [ -n "$diff_output" ]; then
        # Files have changed (modified or added), send a notification
        notify-send "File Change" "Files in the directory have been updated or added."
    fi

    # Update the initial file list with the current one for the next iteration
    cp "$TEMP_DIR/surfshark_files_current.txt" "$TEMP_DIR/surfshark_files_initial.txt"

    # Wait for some time before checking again (e.g., every hour)
    sleep 3600
done
